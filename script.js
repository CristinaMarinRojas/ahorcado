//Palabras disponibles del juego
var  palabras;

var palabras = ['sorteo','misericordia','caballo','trueno','relampago','acecinar','falcado','acordeon',
'ordenador','counter','lol','nociram','elark','ahorcado','callofduty','lloreria','llorodromo','manzana','tortuga',
'medak','jaime','noviadejaime','hormiga','pantalla','proyector','pizarra','comunismo','socialismo','capitalismo','anarquia',
'esquimal','libreta','oso','zapato','pantalon','sacapuntas','raton','espejo','electrizante','resiliencia','hermosa',
'prevalencia','caracteristica','vida','cosa','duda','metodologia','esperanza','herramienta','naturaleza','tristeza',
'estructura','suelta','ecosistema','encuentra','sistema','idea','guia','causa','respuesta','ambivalencia','consecuencia',
'medida','karma','montaña','tecnologia','circunstancia','ropa','perspectiva','muestra','cuenta','competencia','estrategia',
'carta','fuerza','problema','iniciativa','conjetura','armonia','importancia','prueba','bella','experiencia','día','regla',
'presencia','esencia','alma'];

//Letras que el usuario ya ha dicho
var basura=[];

//Palabra elegida aleatoriamente
var palabra = palabras[Math.floor(Math.random()*palabras.length)];

var palabra_oculta = palabra_guiones(palabra);

console.log(palabra);

//Cronometro
h = 0;
m = 0;
s = 0;

//Esta variable es provisional para poder controlar cuando usamos la función checkWin().
var intentos = 0;

window.onload = function(){

    //Metemos la Palabra Misteriosa en el cajon
    meter_palabraoculta();

    //Iniciamos Cronometro 
    setInterval(escribir,1000);

    var audioFondo = new Audio('audio/hanged.mp3');
	var botonPlay = document.querySelector('#playM');
	var botonPause = document.querySelector('#pauseM');

	botonPlay.addEventListener("click", function(){
		audioFondo.play();
		audioFondo.loop = true;
	});

	botonPause.addEventListener("click", function(){
		audioFondo.pause();
	});

	audioFondo.pause();
    audioFondo.currentTime = 0; 
    
    alinearMedio();

}

function alinearMedio(){

    let padre = document.querySelector("#padre");
    let estilo = window.getComputedStyle(padre);

    padre.style.marginTop = ((parseInt(window.innerHeight) - parseInt(estilo.getPropertyValue('height')))/2)+'px';

    console.log(estilo.getPropertyValue('margin-top'));
}

/*--------------------------------------------------------TRANSFORMO LA PALABRA EN GUIONES------------------------------------------ */

function palabra_guiones(palabra){
    var guiones = [];
    var tam = palabra.split("").length;

    for (let i = 0; i < tam; i++) {
        guiones.push('_');
    }

    return guiones;
}



/*--------------------------------------------------------METER PALARA OCULTA EN CAJON---------------------------------------------- */

function meter_palabraoculta(){

    //Me voy al elemento donde lo meter
    var elemento = document.querySelector("input[name='palabra']");

    //Introduzco la palabra dentro del input 
    let tam = palabra_oculta.length;

    let coso = "";

    for (let i = 0; i < tam; i++) {
        coso += palabra_oculta[i]+" ";
    }

    elemento.value = coso;
}

/*--------------------------------------------------------------------CRONOMETRO-----------------------------------------------------*/

function escribir(){
    var h_actual, m_actual, s_actual;
    s++;
    if (s>59){m++;s=0;}
    if (m>59){h++;m=0;}
    if (h>24){h=0;}

    if (s<10){s_actual="0"+s;}else{s_actual=s;}
    if (m<10){m_actual="0"+m;}else{m_actual=m;}
    if (h<10){h_actual="0"+h;}else{h_actual=h;}

    document.getElementById("hms").innerHTML = h_actual + ":" + m_actual + ":" + s_actual; 
}

/*--------------------------------------------------------------------JUGAR-----------------------------------------------------*/

function jugar(){

    //Cada vez que se ejecute esta función significa que ha gastado un intento.

    //Me voy al elemento que quiero y saco su valor
    var letra = document.querySelector("input[name='letra']").value;

    letra = letra.toLowerCase();

    document.querySelector("input[name='letra']").value = "";
    
    //Comprobamos si es un numero o una letra
    var letters = /[A-Za-z]/;
    if (!letra.match(letters)) {
        alert("Tienes que poner una letra. ("+letra+")no es una letra.");

    }else{
        //En este caso ya sabemos que es una letra.

        //Comprobamos si esa letra ya ha salido
        var esta = en_basura(letra);

        if (esta){
            alert("Esa letra ya la has dicho. Dime otra, te perdono el intento");
        }else{
            //La letra la metemos en la basura por si luego más tarde la repetimos
            basura.push(letra);
        
            //En este caso la letra no se había dicho antes, pero tenemos que mirar si esta dentro de mi palara misteriosa
            esta = en_palabra(letra);

            if (esta){
                //alert("Esta en la palabra");
                meter_palabraoculta()

            }else{
                //alert("No esta en la palabra")
                meter_texarea(letra);
                //El limite de intentos es de 7.
                intentos ++;

                changeMonigote();
            }
            
        }
   
    }


    //En cualquiera de los casos anteriores volvemos a meter la palabra misteriosa en el input por si ha tenido una modificación
    meter_palabraoculta();

    //En esta parte se comprobaria si se ha ganado o si se ha perdido y en cualquiera de los dos casos preguntar si quiere volver a jugar

    let action;

    if(intentos == 7){ //si llega al límite de intentos pierde
        action = "img/perder.jpg";   
        crearPopup(action);   
    }else{
        if(checkWin()){ //si ha ganado salta la foto de win, en caso contrario o hace nada sigue hasta que se quede sin intentos
            action = "img/altoahi.jpg";
            crearPopup(action);
        }
    }
    
}

/*---------------------------------------------CAMBIAMOS LA IMAGEN DEL MONIGOTE CUANDO FALLA-------------------------------------------------*/

function changeMonigote(){
    let foto = document.querySelector('#monigote');

    foto.src = "img/ahor"+(intentos+1)+".png";
}

/*---------------------------------------------CREAMOS EL POP UP QUE SALE AL ACABAR LA PARTIDA-------------------------------------------------*/

function crearPopup(action){

	//Creamos el mensaje que sale después de perder o ganar
	var ventana = document.createElement('div');
	ventana.setAttribute('onclick','borrar()');
	ventana.style.backgroundImage = 'url('+action+')';
	ventana.setAttribute('id','capa');
	document.body.appendChild(ventana);
}

//La siguiente función borra la capa creada 

function borrar(){
	var capa = document.querySelector('#capa');

	document.body.removeChild(capa);

	restartAll();
}


//Función para reiniciar todo, borramos lo que haya en los campos y volvemos a generar una palabra.
//También reiniciamos el crono.
//Necesitamos los console.log() para poder ver la palabra nueva que se ha generado.

function restartAll(){

    document.querySelector("input[name='palabra']").value = "";

    document.querySelector("textarea").value = "";

    document.querySelector("input[name='letra']").value = "";

    palabra = palabras[Math.floor(Math.random()*palabras.length)];

    palabra_oculta = palabra_guiones(palabra);

    meter_palabraoculta();

	h = 0;
	m = 0;
    s = 0;

    basura=[];
    
    document.querySelector('#monigote').src = "img/ahor1.png";
    
}

/*---------------------------------------------EN BASURA ( LETRAS QUE EL JUGADOR HA DICHO)-------------------------------------------------*/

function en_basura(letra){
    //Lo ponemos como falso ya que en un primer momento la basura estara vacia
    var resultado = false;
    var tam = basura.length;

    for (let i=0;i<tam && !resultado;i++){
        if (basura[i]== letra){
            resultado = true;
        }
    }

    return resultado;
}


/*---------------------------------------------EN PALABRA (COMPROBAMOS SI ESTA EN LA PALABRA)-------------------------------------------------*/

function en_palabra(letra){
    //Lo ponemos como falso en un principio
    var resultado = false;

    //Separo en un nuevo array la palabra 
    let arraypalabra= palabra.split("");
    //console.log(arraypalabra);

    //Tamaño de la palabra
    var tam = arraypalabra.length;

    for (var i = 0;i<tam;i++){
        console.log(arraypalabra[i] );
        if (arraypalabra[i] == letra){
            palabra_oculta[i] = letra;  //AQUÍ TAMBIÉN MODIFICO LA PALABRA OCULTA
            resultado = true;
        }
    }

    return resultado;
}



/*--------------------------------------------------------METER LETRA EN EL TEXAREA---------------------------------------------- */

function meter_texarea(letra){

    //En el texarea se añadira la letra que nos hemos traido más aquellas que estan ya dentro
    
    var contenido = document.querySelector("textarea").value;
    var contenedor = document.querySelector("textarea");

    if(contenido == ""){
        contenedor.value = letra; 
    }else{
        contenedor.value = contenido + '-' + letra; 
    }
}

/*------------------------------------------------------ CHECK WIN --------------------------------------------------------------- */

//En esta función comprobamos si la palabra oculta es igual a la palbra generada.

function checkWin(){
    if(palabra === palabra_oculta.join("")){
        return true;
    }else{
        return false;
    }
}
